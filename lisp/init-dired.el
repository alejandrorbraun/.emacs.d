(use-package dired+ :straight t)

(global-set-key "\C-x\C-d" 'dired)

(provide 'init-dired)
