(use-package org-ref :straight t)
(use-package ox-hugo :straight t)
(use-package htmlize :straight t)

(with-eval-after-load 'ox
  (require 'ox-hugo))

(with-eval-after-load 'org
  (define-key org-mode-map (kbd "C-c C-q") 'org-set-tags)
  (require 'ox)
  (require 'ox-hugo)
  (require 'ox-org)
  )


(setq org-image-actual-width nil) ;; Allows for setting image sizes directly
(setq org-latex-compiler "xelatex")
(setq org-latex-to-pdf-process 
  '("xelatex -interaction nonstopmode %f"
     "xelatex -interaction nonstopmode %f")) ;; for multiple passes

(add-hook 'org-mode-hook
          (lambda ()
            (local-set-key (kbd "\C-ci") 'org-insert-todo-heading)))

(global-set-key "\C-cn" 'org-capture)


(global-set-key "\C-xa" 'org-agenda)
;; Capture Templates
(setq org-capture-templates
 '(("t" "Todo" entry (file+headline "~/Projects/notes/gtd.org" "Tasks")
        "* TODO %?\n  %i\n")
   ("n" "Doc Notes" entry (file "~/Projects/notes/doc_notes.org")
    "* Notes\n:PROPERTIES:\n:Document: %f\n:Location: %^{Location|%a}\n:TIMESTAMP: %T\n:END:\n** Thoughts\n%?%i")
 ("g" "General" entry (file "~/Projects/notes/general.org")
"* %^{Topic|Programming|Emacs|Life|Other}
** Note
%?")))

(org-babel-do-load-languages
 'org-babel-load-languages '(
			     (python . t)
			     (shell . t)))

(setq org-babel-python-command "python3")
(setq org-confirm-babel-evaluate nil)


(eval-after-load 'org
  '(progn
  (add-to-list 'org-structure-template-alist
	       '("m" "#+BEGIN_SRC python :results output :session a\n\?\n#+END_SRC"))))

(add-to-list 'org-latex-packages-alist '("" "booktabs"))


;; see org-ref for use of these variables
(setq org-ref-bibliography-notes "~/Dropbox/bibliography/notes.org"
      org-ref-default-bibliography '("~/Dropbox/bibliography/references.bib")
      org-ref-pdf-directory "~/Dropbox/bibliography/bibtex-pdfs/")
; ;; END Org-ref customization

;; START ORG SRC customization
(setq-default indent-tabs-mode nil)
;; END ORG SRC customization


(provide 'init-org)
