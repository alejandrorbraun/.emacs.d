(use-package vue-mode :straight t :ensure t :config (add-hook 'vue-mode-hook #'lsp))
(use-package lsp-mode
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         (vue-mode . lsp-deferred)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp-deferred)

(use-package lsp-ui :commands lsp-ui-mode :straight t)
;; if you are helm user
(use-package helm-lsp :commands helm-lsp-workspace-symbol :straight t)

;; optionally if you want to use debugger
(use-package dap-mode :straight t)

;; optional if you want which-key integration
(use-package which-key :straight t
    :config
    (which-key-mode))

(straight-use-package
 '(lsp-volar :type git :host github :repo "jadestrong/lsp-volar"))

(use-package lsp-volar
  :straight t :ensure t)

(straight-use-package
 '(lsp-tailwindcss :type git :host github :repo "merrickluo/lsp-tailwindcss"))


(define-key company-mode-map (kbd "<tab>") 'company-complete)
(provide 'init-vue)

