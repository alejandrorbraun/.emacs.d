(use-package elpy
  :straight t
  :ensure t
  :init
  (elpy-enable)
  )



(provide 'init-python)
