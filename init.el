(setq package-enable-at-startup nil)
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "lib/lsp-mode" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "lib/lsp-mode/clients" user-emacs-directory))
(require 'init-straight)
(require 'init-general)
(require 'init-dired)
(require 'init-pdf)
(require 'init-shell)
(require 'init-python)
(require 'init-vue)
(require 'init-org)
(require 'init-appearance)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("02591317120fb1d02f8eb4ad48831823a7926113fa9ecfb5a59742420de206e0" default))
 '(org-agenda-files '("~/Projects/notes/gtd.org")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
